Feature: Bank Application Create Account
	As A user I wish to create accounts
	
	Scenario Outline: Create An Account
		Given a user is logged in at the bank page wanting to go to create account with "<username>" "<password>"
		When a user clicks onto the profile link
		When they are redirected to the profile page
		When they click on the create account link
		When they input the accout type "<accounttype>"
		When they input the branch "<branch>"
		Then they submit the account create
	Examples:
		| username     | password  |accounttype|branch|
		| johnlao      | password  |1|3|
		| johnlao      | password  |2|2|
		| johnlao      | password  |2|3|
		| leooo        | password  |1|1|
		| leooo        | password  |2|2|
		| leooo        | password  |1|3|
		

		