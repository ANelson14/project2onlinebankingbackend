Feature: Fund Transfer
	As a user I want to transfer funds between my accounts
	Scenario Outline: Make a Fund Transfer
		Given a user is logged in at the bank page wanting to go to Fund Transfer with "<username>" "<password>"
		When a user clicks the fund transer link
		And they are redirected to the fund transer page
		And they select an account number for fund transer "<accountNum1>" "<accountNum2>"
		And they input a fund transer amount "<amount>"
		Then the user submits the fund transer

	Examples:
		| username     | password  | accountNum1   | accountNum2|amount |
		| starjohnson  | password  |132548102      |132548101   |14     |
		| johnlao      | password  |0300001001     |0300001002  | 10    |
		| leooo        | password  |0300002001     |0200002002  | 10    |