Feature: Bank Application Logout
	As A user I wish to log out of the banking application
	
	Scenario Outline: Log out
		Given a user is logged in at the bank page wanting to logout "<username>" "<password>"
		When a user clicks the logout link
		Then they are redirected back to the login page

	Examples:
		| username     | password  |
		| johnlao      | password  |
		| leooo        | password  | 
		| starjohnson  | password  |