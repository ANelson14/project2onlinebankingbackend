package com.example.controller;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.model.Account;
import com.example.model.AccountType;
import com.example.model.Branch;
import com.example.model.ClientUser;
import com.example.model.Transaction;
import com.example.service.AccountService;
import com.example.service.AccountTypeService;
import com.example.service.BranchService;
import com.example.service.ClientUserService;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

@RestController
@RequestMapping(value="/api/bank")
@AllArgsConstructor(onConstructor=@__(@Autowired))
@NoArgsConstructor
//@CrossOrigin(origins = "http://localhost:4200")
@CrossOrigin(origins = "*")
public class AccountController {
	private AccountService aServ;
	private BranchService branchService;
	private AccountTypeService accountTypeService;
	private ClientUserService clientUserService;
	
	@PostMapping("/transfer")
	public ResponseEntity<String> fundTransfer(@RequestBody LinkedHashMap<String, String> fMap )
	{
		String accFrom = fMap.get("acc1");
		String accTo = fMap.get("acc2");
		double amt = Double.parseDouble(fMap.get("amount"));
		System.out.println("Account Number from " + accFrom);
		System.out.println("Account Number To " +accTo);
		System.out.println("Balance to transfer " + amt);
		aServ.transferFunds(accFrom, accTo, amt);
		return new ResponseEntity<>("Transfer Complete", HttpStatus.ACCEPTED);
		
	}
	
	@PostMapping("/profile/createaccount")
	public ResponseEntity<String> insertAccount(@RequestBody LinkedHashMap<String, String> acMap){
		double balance = 0.00;
//		System.out.println(acMap);
		int clientId = Integer.parseInt(acMap.get("clientId"));
		int acTypeId =  Integer.parseInt(acMap.get("accountType"));
		int branchId = Integer.parseInt(acMap.get("branch"));
		
		String newClientId = intToString(clientId, 5);
		String newBranchId = intToString(branchId, 2);
		String newTypeId = intToString(acTypeId, 3);
		
		
		String newAccount = newBranchId+newClientId+newTypeId;
		
		ClientUser user = clientUserService.getClientByClientId(clientId);
		Branch branchTypeObj = branchService.getById(branchId);
		AccountType accTypeObj = accountTypeService.getById(acTypeId);
		List<Transaction> tList = new ArrayList<>();
		Account account = new Account(newAccount, balance , user , accTypeObj, branchTypeObj, tList);
		aServ.insertAccount(account);
	return new ResponseEntity<>("new account created", HttpStatus.CREATED);
	}
	
		public static String intToString(int num, int digits) {
	    String output = Integer.toString(num);
	    while (output.length() < digits) output = "0" + output;
	    return output;
		}
}
