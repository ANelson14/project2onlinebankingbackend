package com.example.service;

import java.sql.Timestamp;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.model.Account;
import com.example.model.Transaction;
import com.example.repository.AccountRepository;
import com.example.repository.TransactionRepository;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

@Service
@AllArgsConstructor(onConstructor=@__(@Autowired))
@NoArgsConstructor 
public class TransactionService {
	
	private AccountRepository aRepo;
	private TransactionRepository transactionRepository;
	
	public void insert(Transaction t)
	{
		transactionRepository.save(t);
	}

	public List<Transaction> getAllTransactionsByClientId(String id) {
		Account a = aRepo.findByAccountNumber(id);
		return transactionRepository.findByAccount(a);
	}

	public List<Transaction> getTransactionByAccount(String account) {
		
		Account acc = aRepo.findByAccountNumber(account);
//		return transactionRepository.findByAccount(acc);
		return transactionRepository.findByAccount(acc);
	}
	public String withdraw(String account_number, double debitAmount,String description) {
	    
		Account a = aRepo.findByAccountNumber(account_number);
		double balance = a.getBalance();
	//	   System.out.println("balance before transaction " + balance);
		   if((a.getBalance()-debitAmount)<0) 
			  return "you dont have enough funds";
		   else {
		   balance = balance - debitAmount;
		   
		   a.setBalance(balance);
		//   System.out.println("balance after transaction " + balance);
		   Transaction tr = new Transaction(description, new Timestamp(System.currentTimeMillis()), debitAmount,0,a.getBalance(),
					 a); 
		   
	         transactionRepository.save(tr);
	         return "Transaction succsessful";
		   }
		
	}

	
	public String deposit(String account_number, double creditAmount,String description) {

		Account a = aRepo.findByAccountNumber(account_number);
		double balance = a.getBalance();
		System.out.println("balance before transaction " + balance);
		if((a.getBalance()+creditAmount)<0) 
			return "cannot have a negative balance";
		   balance = balance + creditAmount;
	//	   System.out.println("balance after transaction " + balance);
		   a.setBalance(balance);
		   Transaction tr =  new Transaction(description, new Timestamp(System.currentTimeMillis()), 0,creditAmount,a.getBalance(),
					 a); 
		   
	         transactionRepository.save(tr);
	         return "Transaction succsessful";
		
	}
	
	public String closeAccount(String account_number) {
		Account a = aRepo.findByAccountNumber(account_number);
		if(a.getBalance()>0)
			 return "Account is not empty, need to withdraw all the money before closing";
		else
			aRepo.delete(a);
		
		  return "Account successfully deleted";
		
	}
	
}

