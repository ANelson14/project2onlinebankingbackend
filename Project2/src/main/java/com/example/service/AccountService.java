package com.example.service;

import java.sql.Timestamp;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.model.Account;
import com.example.model.Transaction;
import com.example.repository.AccountRepository;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

@Service
@AllArgsConstructor(onConstructor = @__(@Autowired))
@NoArgsConstructor
public class AccountService {

	private TransactionService tService;
	private AccountRepository accountRepository;


	public Account getAccountByAccountNumber(String accNumFrom) { // change back to string later
		return accountRepository.findByAccountNumber(accNumFrom);
	}
	
	//creating the new account 
	public void insertAccount(Account account) {
		accountRepository.save(account);
//		return account;
	}

	public void transferFunds(String accNumFrom, String accNumTo, double amount) {
		System.out.println("in the transfer methods");
		Account from = getAccountByAccountNumber(accNumFrom);
		Account to = getAccountByAccountNumber(accNumTo);
		if (from == null || to == null) {
		} else {
			if (from.getBalance() - amount < 0.00) {
				System.out.println("Overdraft fees will be applied");
			} else {
				System.out.println("Initiating fund transfer");
				
				from.setTransList(tService.getAllTransactionsByClientId(accNumFrom));
				to.setTransList(tService.getAllTransactionsByClientId(accNumTo));
				from.setBalance(from.getBalance() - amount);
				double fromcurrBalance = from.getBalance();
				List<Transaction> fTr = from.getTransList();
				Transaction amountWithdrawn = new Transaction("Transfer of money from " + from.getTHolder().getTypeName() + " to "	+ to.getTHolder().getTypeName(),new Timestamp(System.currentTimeMillis()), amount, 0, fromcurrBalance, from);
				fTr.add(amountWithdrawn);
				if (fTr.size() == 0) {
					tService.insert(amountWithdrawn);
				} else {
					
					
					fTr.add(amountWithdrawn);
					tService.insert(amountWithdrawn);
				}
				accountRepository.save(from);
				to.setBalance(to.getBalance() + amount);

				double toCurrentBalance= to.getBalance();
				Transaction amountDeposited = new Transaction(
						"Transfer of money from " + from.getTHolder().getTypeName() + " to " + to.getTHolder().getTypeName(),new Timestamp(System.currentTimeMillis()), 0, amount,toCurrentBalance, to);
				List<Transaction> tTr = to.getTransList();
				if (tTr.size() == 0) {

					tTr.add(amountDeposited);
					tService.insert(amountDeposited);
				} else {
					tTr.add(amountDeposited);
				}
				accountRepository.save(to);
			}
		}
	}
}
