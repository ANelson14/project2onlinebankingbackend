package com.example.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter @Setter
@NoArgsConstructor
@AllArgsConstructor

@Entity
@Table(name="Branch")
public class Branch {
	@Id
    @Column(name = "branch_id")
//	@Setter(AccessLevel.NONE)
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int branchId;
	
	@Column(name="branch_name")
	private String branchName;
	
	@OneToMany(mappedBy="bHolder", fetch=FetchType.EAGER)
	@JsonIgnore
	private List<Account> accList = new ArrayList<>();

	public Branch(String branchName) {
		super();
		this.branchName = branchName;
	}

	@Override
	public String toString() {
		return "Branch [branchName=" + branchName + "]";
	}

	
	
}
