 package com.example.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter @Setter
@NoArgsConstructor
@AllArgsConstructor

@Entity
@Table(name="Account")
public class Account {
	@Id
    @Column(name = "account_number")
//	@Setter(AccessLevel.PUBLIC)

	@Setter(AccessLevel.NONE)

    private String accountNumber; //change back to string later
	
	@Column(name="balance")
	private double balance;
	
	@ManyToOne(fetch=FetchType.EAGER )
	@JoinColumn(name="client_fk")
	@JsonIgnore
	private ClientUser cHolder;
	
	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="type_fk")
	private AccountType tHolder;
	
	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="branch_fk")
	private Branch bHolder;
	
	@OneToMany(mappedBy="account", fetch=FetchType.LAZY,  cascade = CascadeType.ALL)
		private List<Transaction> transList = new ArrayList<>();

	public Account(String accountNumber, double balance, ClientUser cHolder, AccountType tHolder, Branch bHolder) {
		super();
		this.accountNumber = accountNumber;
		this.balance = balance;
		this.cHolder = cHolder;
		this.tHolder = tHolder;
		this.bHolder = bHolder;
	}
	
	@Override
	public String toString() {
		return "Account [accountNumber=" + accountNumber + ", balance=" + balance + ", tHolder=" + tHolder
				+ ", bHolder=" + bHolder + ", transList=" + transList + "]";
	}
	 
	
}

