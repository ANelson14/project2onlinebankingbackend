package com.example.model;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;


@Getter @Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name="Transaction")
public class Transaction {
	@Id
	@Column(name="transaction_id")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Setter(AccessLevel.NONE)
	private int transactionId;
	
	
	@Column(name="description")
	private String description;
	
	@JsonFormat(pattern = "yyyy-MM-dd, HH:mm")
	@Column(name="Transaction_date_time")
	private Timestamp  TransactionDateTime;
	
	@Column(name="debit_amount")
	private double debitAmount;
	
	@Column(name="credit_amount")
	private double creditAmount;
	
	@Column(name = "current_Balance")
	private double currentBalance;
	
	@ManyToOne
	@JoinColumn(name="transaction_FK")
	@JsonIgnore
	private Account account;



	public Transaction(String description, Timestamp transactionDateTime, double debitAmount, double creditAmount) {
		super();
		this.description = description;
		TransactionDateTime = transactionDateTime;
		this.debitAmount = debitAmount;
		this.creditAmount = creditAmount;
	}

	public Transaction(String description, Timestamp transactionDateTime, double debitAmount, double creditAmount,
		double currentBalance,	Account accHolder) {
		super();
		this.description = description;
		TransactionDateTime = transactionDateTime;
		this.debitAmount = debitAmount;
		this.creditAmount = creditAmount;
		this.currentBalance = currentBalance;
		this.account = accHolder;
	}

	@Override
	public String toString() {
		return "Transaction [transactionId=" + transactionId + ", description=" + description + ", TransactionDateTime="
				+ TransactionDateTime + ", debitAmount=" + debitAmount + ", creditAmount=" + creditAmount
				+ ", currentBalance=" + currentBalance + "]";
	}
	
	
}

