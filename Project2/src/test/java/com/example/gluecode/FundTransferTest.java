package com.example.gluecode;

import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.example.e2e.page.Bank;
import com.example.e2e.page.BankLogin;
import com.example.e2e.page.Deposit;
import com.example.e2e.page.FundTransfer;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

public class FundTransferTest {
	public BankLogin bl;
	public Bank b;
	public FundTransfer fs;
	@Given("a user is logged in at the bank page wanting to go to Fund Transfer with {string} {string}")
	public void a_user_is_logged_in_at_the_bank_page_wanting_to_go_to_fund_transfer_with(String string, String string2) {
		bl = new BankLogin(BankDriverUtility.driver);
		b = new Bank(BankDriverUtility.driver);
		fs = new FundTransfer(BankDriverUtility.driver);
		bl.username.sendKeys(string);
		bl.password.sendKeys(string2);
		bl.loginButton.click();
		WebDriverWait wait = new WebDriverWait(BankDriverUtility.driver, 6);
	    wait.until(ExpectedConditions.urlMatches("http://localhost:4200/bank"));

	}

	@When("a user clicks the fund transer link")
	public void a_user_clicks_the_fund_transer_link() {
		b.fundTransferLink.click();
	}
	@When("they are redirected to the fund transer page")
	public void they_are_redirected_to_the_fund_transer_page() {
		WebDriverWait wait = new WebDriverWait(BankDriverUtility.driver, 6);
	    wait.until(ExpectedConditions.urlMatches("http://localhost:4200/bank/transfer"));
	}
	@When("they select an account number for fund transer {string} {string}")
	public void they_select_an_account_number_for_fund_transer(String string, String string2) {
		fs.getSelectOptions1().selectByValue(string);
		fs.getSelectOptions2().selectByValue(string2);
		
	}
	@When("they input a fund transer amount {string}")
	public void they_input_a_fund_transer_amount(String string) {
		fs.Amt.sendKeys(string);
	}
	@Then("the user submits the fund transer")
	public void the_user_submits_the_fund_transer() {
	    fs.subButton.click();
	}
}
