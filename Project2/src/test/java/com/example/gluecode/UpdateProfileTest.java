package com.example.gluecode;

import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.example.e2e.page.Bank;
import com.example.e2e.page.BankLogin;
import com.example.e2e.page.Profile;
import com.example.e2e.page.UpdateProfile;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

public class UpdateProfileTest {

	public BankLogin bl;
	public Bank b;
	public Profile p;
	public UpdateProfile up;

	@Given("a user is at the bank page ready to update profile {string} {string}")
	public void a_user_is_at_the_bank_page_ready_to_update_profile(String string, String string2) {
		bl = new BankLogin(BankDriverUtility.driver);
		b = new Bank(BankDriverUtility.driver);
		p = new Profile(BankDriverUtility.driver);
		up = new UpdateProfile(BankDriverUtility.driver);
		bl.username.sendKeys(string);
		bl.password.sendKeys(string2);
		bl.loginButton.click();
		WebDriverWait wait = new WebDriverWait(BankDriverUtility.driver, 6);
		wait.until(ExpectedConditions.urlMatches("http://localhost:4200/bank"));
	}

	@When("a user click profile button")
	public void a_user_click_profile_button() {
		b.profile.click();
		System.out.println(this.b.profile.getText());
	}

	@When("a user is redirect to profile page")
	public void a_user_is_redirect_to_profile_page() {
		WebDriverWait wait = new WebDriverWait(BankDriverUtility.driver, 60);
		wait.until(ExpectedConditions.urlMatches("http://localhost:4200/bank/profile"));
	}

	@When("a user clicks update profille")
	public void a_user_clicks_update_profille() {
		p.profileupdatelink.click();
		WebDriverWait wait = new WebDriverWait(BankDriverUtility.driver, 60);
		wait.until(ExpectedConditions.urlMatches("http://localhost:4200/bank/profile/updateprofile"));
	}

	@When("a user inputs first a name {string}")
	public void a_user_inputs_first_a_name(String string) {
		up.firstName.sendKeys(string);
	}

	@When("then a user inputs a last name {string}")
	public void then_a_user_inputs_a_last_name(String string) {
		up.lastName.sendKeys(string);
	}

	@When("then a user inputs a contact Number  {string}")
	public void then_a_user_inputs_a_contact_number(String string) {
		up.contactNumber.sendKeys(string);
	}

	@When("then a user inputs a address  {string}")
	public void then_a_user_inputs_a_address(String string) {
		up.address.sendKeys(string);
	}

	@When("then a user inputs ancfh email {string}")
	public void then_a_user_inputs_ancfh_email(String string) {
		up.email.sendKeys(string);
	}

	@Then("then submits the updated profile information")
	public void then_submits_the_updated_profile_information() {
		up.subButton.click();
	}

}
