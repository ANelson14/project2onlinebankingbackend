package com.example.gluecode;

import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.example.e2e.page.Bank;
import com.example.e2e.page.BankLogin;
import com.example.e2e.page.CreateAccount;
import com.example.e2e.page.Profile;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

public class CreateAccountTest {

	public BankLogin bl;
	public Bank b;
	public Profile p;
	public CreateAccount ca;

	@Given("a user is logged in at the bank page wanting to go to create account with {string} {string}")
	public void a_user_is_logged_in_at_the_bank_page_wanting_to_go_to_create_account_with(String string,
			String string2) {
		bl = new BankLogin(BankDriverUtility.driver);
		b = new Bank(BankDriverUtility.driver);
		p = new Profile(BankDriverUtility.driver);
		ca = new CreateAccount(BankDriverUtility.driver);
		bl.username.sendKeys(string);
		bl.password.sendKeys(string2);
		bl.loginButton.click();
		WebDriverWait wait = new WebDriverWait(BankDriverUtility.driver, 6);
		wait.until(ExpectedConditions.urlMatches("http://localhost:4200/bank"));
	}

	@When("a user clicks onto the profile link")
	public void a_user_clicks_onto_the_profile_link() {
		b.profile.click();
		System.out.println(this.b.profile.getText());
		WebDriverWait wait = new WebDriverWait(BankDriverUtility.driver, 60);
		wait.until(ExpectedConditions.urlMatches("http://localhost:4200/bank/profile"));
	}

	@When("they click on the create account link")
	public void they_click_on_the_create_account_link() {
		p.createAccountlink.click();
		;
		WebDriverWait wait = new WebDriverWait(BankDriverUtility.driver, 60);
		wait.until(ExpectedConditions.urlMatches("http://localhost:4200/bank/profile/createaccount"));
	}

	@When("they input the accout type {string}")
	public void they_input_the_accout_type(String string) {
		ca.getSelectOptions1().selectByValue(string);
	}

	@When("they input the branch {string}")
	public void they_input_the_branch(String string) {
		ca.getSelectOptions2().selectByValue(string);
	}

	@Then("they submit the account create")
	public void they_submit_the_account_create() {
		ca.subButton.click();
	}
	
}
