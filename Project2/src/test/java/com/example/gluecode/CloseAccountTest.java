package com.example.gluecode;

import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.example.e2e.page.Bank;
import com.example.e2e.page.BankLogin;
import com.example.e2e.page.CloseAccount;
import com.example.e2e.page.Profile;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

public class CloseAccountTest {

	public BankLogin bl;
	public Bank b;
	public Profile p;
	public CloseAccount ca;
@Given("a user is logged in at the bank page wanting to go to close account with {string} {string}")
public void a_user_is_logged_in_at_the_bank_page_wanting_to_go_to_close_account_with(String string, String string2) {
	bl = new BankLogin(BankDriverUtility.driver);
	b = new Bank(BankDriverUtility.driver);
	p = new Profile(BankDriverUtility.driver);
	ca = new CloseAccount(BankDriverUtility.driver);
	bl.username.sendKeys(string);
	bl.password.sendKeys(string2);
	bl.loginButton.click();
	WebDriverWait wait = new WebDriverWait(BankDriverUtility.driver, 6);
	wait.until(ExpectedConditions.urlMatches("http://localhost:4200/bank"));
}


@When("a user clicks onto th profile link")
public void a_user_clicks_onto_th_profile_link() {
	b.profile.click();
	System.out.println(this.b.profile.getText());
}
@When("they are redirected to the profiles page")
public void they_are_redirected_to_the_profiles_page() {
	WebDriverWait wait = new WebDriverWait(BankDriverUtility.driver, 60);
	wait.until(ExpectedConditions.urlMatches("http://localhost:4200/bank/profile"));
}
@When("they click on the close account link")
public void they_click_on_the_close_account_link() {
    p.closeAccountlink.click();
    WebDriverWait wait = new WebDriverWait(BankDriverUtility.driver, 60);
	wait.until(ExpectedConditions.urlMatches("http://localhost:4200/bank/profile/closeaccount"));
}
@When("they input the accout number {string}")
public void they_input_the_accout_number(String string) {
    ca.getSelectOptions1().selectByValue(string);
}
@Then("they submit the account close")
public void they_submit_the_account_close() {
	ca.subButton.click();
}


}
