package com.example.eval.controller;

import static org.hamcrest.CoreMatchers.is;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import com.example.controller.TransactionController;
import com.example.model.Account;
import com.example.model.AccountType;
import com.example.model.Branch;
import com.example.model.ClientUser;
import com.example.model.Transaction;
import com.example.service.TransactionService;
import com.fasterxml.jackson.databind.ObjectMapper;

@SpringBootTest
public class TransactionControllerUnitTest {
	
	@Mock
	private TransactionService tranServ;
	
	@InjectMocks
	private TransactionController tranCon;
	
	
	private ClientUser cu;
//	private Transaction trans;
	private Transaction trans1;
	private Transaction trans2;
	
	private Account acc;
	private Account acc2;
	private List<Transaction> tList;
	
	private MockMvc mock;
	
	@BeforeEach
	public void setUp() throws Exception{
		cu = new ClientUser("Star", "Johnson", "Space", 55, "starjohnson@maildrop.cc", "5015558888", "starjohnson",
				"password", null, null);
		trans1 = new Transaction("Payday", Timestamp.valueOf("2021-01-28 21:21:52"), 0 , 200, 50000200, acc2);
		trans2 = new Transaction("Purchase of Amazing Figure", Timestamp.valueOf("2021-01-28 21:21:52"), 280 , 0, 49999920, acc2);
		tList = new ArrayList<>();
		tList.addAll(Arrays.asList(trans1,trans2));
		AccountType at1 = new AccountType("checking");
		AccountType at2 = new AccountType("savings");
		Branch b1 = new Branch("Santa Clara");
		Branch b3 = new Branch("San Francisco");
		acc2  = new Account("132548102", 5, cu, at1, b1, tList);
		mock = MockMvcBuilders.standaloneSetup(tranCon).build();
		
		when(tranServ.getTransactionByAccount("132548102")).thenReturn(tList);
		
	}
	
	@Test
	public void getViewTransactionStatement() throws Exception {
		System.out.println(tList);
		mock.perform(get("/api/transaction/view").param("account", "132548102"))
		.andExpect(status().isOk());//.andExpect(jsonPath("$").value(tList));
	}
	
	

}
