package com.example.eval.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;

import com.example.model.ClientUser;
import com.example.repository.ClientUserRepository;
import com.example.service.AccountService;
import com.example.service.ClientUserService;


@SpringBootTest
public class ClientServiceTest {

	@Mock
	private ClientUserRepository cuRepo;
	
	private ClientUserService cuServ;
	
	private ClientUserService clientServ = mock(ClientUserService.class);
	
	@InjectMocks
	private ClientUserService mcuServ;
	
	private ClientUser cu1;
	private ClientUser cu2;
	private ClientUser cu3;
	private ClientUser cu4;
	private List<ClientUser> cList;
	
	@BeforeEach
	public void Setup() throws Exception {
		mcuServ = mock(ClientUserService.class);
		MockitoAnnotations.initMocks(this);
		cuServ = new ClientUserService(cuRepo);
		cu1 = new ClientUser("john","lao","santa paula", 23 ,"laojohnmatthew@gmail.com", "2246237364", "johnlao", "pass", null, null);
		cu2 = new ClientUser("john","leo","santa paula", 24 ,"leojohnmatthew@gmail.com", "2246237364", "leooo","pass", null, null);
		cu4 = new ClientUser("Chirenjeebi","Parajuli","Dallas", 24 ,"cp@gmail.com", "1001000111", "leooo","pass", null, null);
		cu3 = new ClientUser("Star", "Johnson", "Space", 55,"starjohnson@galcticprotectorate.com","5015558888", "starjohnson","password", null, null);
		cList = new ArrayList<>();
		cList.addAll(Arrays.asList(cu1,cu2,cu3,cu4));
		when(cuRepo.findByUsername("johnlao")).thenReturn(cu1);
		when(cuRepo.findByUsername("nope")).thenReturn(null);
		when(cuRepo.findAll()).thenReturn(cList);
//		doNothing().when(cuServ).updateClient(cu4);
		
	}
	
	@Test
	public void testFindByNameSuccess() {
		assertEquals(cuServ.getClientByUsername("johnlao"), cu1);
	}
	
	@Test
	public void testFindByNameFailure() {
		assertEquals(cuServ.getClientByUsername("nope"), null);
	}
	
	@Test
	public void testFindAllSuccess() {
		assertEquals(cuServ.getAllClient(), cList);
	}
	
	@Test
	public void testInsertClientSuccess() {
//		assertEquals(mcuServ.insertClient(cu1), cu1);
		assertEquals(cuServ.insertClient(cu1), cu1);
	}
	
	@Test
	public void testUpdateClientSuccess() {
//		assertEquals(cuServ.updateClient(cu1), cu1);
		doNothing().when(clientServ).updateClient(cu1);
		clientServ.updateClient(cu1);
		verify(clientServ, times(1)).updateClient(cu1);
	}	
}

