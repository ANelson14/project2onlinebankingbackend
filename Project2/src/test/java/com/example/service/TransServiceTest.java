
	
	package com.example.service;

	import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.sql.Timestamp;


import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;

import com.example.model.Account;
import com.example.model.AccountType;
import com.example.model.Branch;
import com.example.model.ClientUser;
import com.example.model.Transaction;
import com.example.repository.AccountRepository;
import com.example.repository.TransactionRepository;

	@SpringBootTest
	public class TransServiceTest {

		@Mock
		private TransactionRepository tRepo;
		
		@Mock
		private AccountRepository aRepo;
		
		@InjectMocks
		private TransactionService tService;
		
		String s = "Transaction succsessful";
		String result="Transaction succsessful";
		String del ="Account is not empty, need to withdraw all the money before closing";
		String del1="Account successfully deleted";
	
		@BeforeEach
		public void SetUp() throws Exception {
			ClientUser c = new ClientUser( "Star", "Johnson", "Space", 55, "starjohnson@maildrop.cc", "5015558888",
					"starjohnson", "password", null, null);
			Branch b3 = new Branch("San Francisco");
			AccountType at2 = new AccountType("savings");
			Account a2 = new Account("132548102", 10000, c, at2, b3); // not auto generated for account no yet. //
			Account a3 = new Account("132548103", 0, c, at2, b3);
			double balance, creditAmount=0;
			balance = a2.getBalance()+creditAmount;
			Timestamp tstmp = Timestamp.valueOf("2021-01-26 10:00:34");
	
			Transaction tr = new Transaction("grocery",tstmp,300,0,1000,a2);
			tRepo.save(tr);
			when(aRepo.findByAccountNumber("132548102")).thenReturn(a2);
			when(aRepo.findByAccountNumber("132548103")).thenReturn(a3);



		}
		
		@Test
		public void withdraw() {
			assertEquals(tService.withdraw("132548102",200,"grocey"),s);
			
		}
		
		@Test
		public void deposit() {
			assertEquals(tService.deposit("132548102",200,"grocey"),s);
		}
		
		@Test
		public void closeAccount() {
			assertEquals(tService.closeAccount("132548103"),del1);
		}
		
	}



