package com.example.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;

import com.example.model.Account;
import com.example.model.AccountType;
import com.example.model.Branch;
import com.example.model.ClientUser;
import com.example.model.Transaction;
import com.example.repository.AccountRepository;
import com.example.repository.TransactionRepository;

@SpringBootTest
public class TServiceTest {

	@Mock
	private TransactionRepository tRepo;
	
	@Mock
	private AccountRepository aRepo;
	
	@InjectMocks
	private TransactionService tService;
	
	List<Transaction> tList = new ArrayList<>();
	@BeforeEach
	public void SetUp() throws Exception {
		ClientUser c = new ClientUser( "Star", "Johnson", "Space", 55, "starjohnson@maildrop.cc", "5015558888",
				"starjohnson", "password", null, null);
		Branch b3 = new Branch("San Francisco");
		AccountType at2 = new AccountType("savings");
		Account a = new Account("132548103", 0, c, at2, b3, tList);
		Account a2 = new Account("132548102", 10000, c, at2, b3, tList); // not auto generated for account no yet. //
		Timestamp tstmp = Timestamp.valueOf("2021-01-26 10:00:34");
		Transaction t = new Transaction(4,"Transfer of money from savings to checking",tstmp, 0, 5000.02, 5005.02, a2 );
		tList.add(t);
		when(aRepo.findByAccountNumber("132548102")).thenReturn(a2);
		when(aRepo.findByAccountNumber("132548103")).thenReturn(a);
		doNothing().when(aRepo).delete(a);
		doNothing().when(aRepo).delete(a2);

		when(tService.getAllTransactionsByClientId("132548102")).thenReturn(tList);
		when(tService.getTransactionByAccount("132548102")).thenReturn(tList);
	}
	@Test
	public void getTransactionListByAccountNumber() {
		assertEquals(tService.getAllTransactionsByClientId("132548102"), tList);
	}
	@Test
	public void deleteSuccess() {
		assertEquals(tService.closeAccount("132548103"), "Account successfully deleted");
	}
	@Test
	public void deleteFailure() {
		assertEquals(tService.closeAccount("132548102"), "Account is not empty, need to withdraw all the money before closing");
	}
	
	@Test
	public void getTransactionStatementByAccountNumber() {
		assertEquals(tService.getTransactionByAccount("132548102"), tList);
	}
	
	
	
}
