package com.example.e2e.page;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class BankRegistration {
//	public static final String title = "Welcome: Mercury Tours";
	
	
	@FindBy (id="register-instruction")
	public WebElement instructionParagraph;
//	Please submit your registration form for new account and E-banking user!
	@FindBy(xpath = "//input[@formcontrolname= 'firstName']")
	public WebElement firstName;
	
	@FindBy(xpath = "//input[@formcontrolname= 'lastName']")
	public WebElement lastName;
	
	@FindBy(xpath = "//input[@formcontrolname= 'contactNumber']")
	public WebElement contactNumber;
	
	@FindBy(xpath = "//input[@formcontrolname= 'address']")
	public WebElement address;
	
	@FindBy(xpath = "//input[@formcontrolname= 'age']")
	public WebElement age;
	
	@FindBy(xpath = "//input[@formcontrolname= 'username']")
	public WebElement username;
	
	@FindBy(xpath = "//input[@formcontrolname= 'password']")
	public WebElement password;
	
	@FindBy(xpath = "//input[@formcontrolname= 'email']")
	public WebElement email;
	
	@FindBy(xpath = "//input[type=file]")
	public WebElement uplaodImage;
	
	@FindBy(xpath = "//button[@type='submit']['register-user']")
	public WebElement registerButton;
	
	@FindBy(xpath = "//input[@type='checkbox']")
	public WebElement agree;
	
//	@FindBy(linkText = "Home")
//	public WebElement homeLink;
	
//	@FindBy(xpath = "//a[text()='SIGN-ON']")
//	public WebElement signOnLink;

	public BankRegistration(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}
	
	
}
