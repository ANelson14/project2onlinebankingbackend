package com.example.e2e.page;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;

public class Withdrawal {
	@FindBy(xpath = "//*[@id='accountNumber']")
	public WebElement account;
	@FindBy(xpath = "/html/body/app-root/app-bank/div/div/app-withdraw/div/form/div[1]/input")
	public WebElement withAmt;
	
	@FindBy(xpath = "/html/body/app-root/app-bank/div/div/app-withdraw/div/form/div[2]/input")
	public WebElement desc;

	@FindBy(xpath = "/html/body/app-root/app-bank/div/div/app-withdraw/div/form/button")
	public WebElement subButton;

	public Select getSelectOptions() {
		return new Select(account);
	}
	public Withdrawal(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}
}
