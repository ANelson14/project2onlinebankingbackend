package com.example.e2e.page;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class BankForgotPass {

	@FindBy(xpath = "//input[@id= 'email']")
	public WebElement email;
	@FindBy(xpath = "//*[@id= 'forgotpassbutton']")
	public WebElement fButton;
	
	public void clickForgot()
	{
		this.fButton.click();
	}
	public BankForgotPass(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}
}
